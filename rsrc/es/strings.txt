STRINGS

AllCertified	Toutes les identités certifiées non-révoquées :
AllCertifiedIO	Débuts (↑) et fins (↓) de validité de toutes les certifications émises :
AllCertifiers	Tous les certificateurs connus et non-révoqués :
AllCertifiersIO	Débuts (↑) et fins (↓) de validité de toutes les certifications reçues :
50allParameters	Informations : Paramètres de la chaîne de blocs
AnswerNb	Nombre maximal de propositions de certificateurs :
AppMLimitDate	Date limite de réadhésion
AppNLimitDate	Date limite de la demande d'adhésion
AppRLimitDate	Date de révocation
Availability	Disponibilité du prochain envoi de certification
Bct	Temps médian de la chaîne de blocs
Block	Bloc
Brackets	Tranches
Calculator	Calculateur de distance et de qualité
23calculator	Propriétés : Calculateur de distance et de qualité
22centralities	Propriétés : Centralités
centralities	Centralités
Centrality	Centralité
certification	certification :
Certifications	Certifications
certifications	certifications :
55certificationsFrom	Informations : Certifications depuis les membres
certificationsFrom	Certifications depuis les membres
56certificationsTo	Informations : Certifications vers les membres
certificationsTo	Certifications vers les membres
Certifiers	Nouveaux certificateurs :
certifiers	les nouveaux certificateurs
34certLim	Limites : Limites des certifications par date
certsNb	^0 certifications
Client	Version client : 
Computation_duration	Durée du calcul
Day	Jour
day	jour(s)
Delay	Durée (jours)
Distance	Règle de distance
DistanceE	Règle de distance attendue
distanceRule	Règle de distance attendue : ^0% (^1 / ^2)
distanceRule0	Règle de distance attendue : ^0%
20distances	Propriétés : Distances (règle de)
distances	Distances (règle de)
DistOfIs	La distance attendue de ^0 est de ^1% : ^2
DistOfUnknownIs	La distance attendue est de ^1% : ^2
Distribution	Distribution
dossiers	dossiers
Duplicate	^0 apparaît plusieurs fois (ligne ^1)
DuplicateLoc	^0 apparaît plusieurs fois (dans ^1, ligne ^2)
en	English
Entry	Entrée de 
es	Español
Exit	Sortie de 
Explorer	Explorateur
1explorer	Explorateur de la toile de confiance
FirstEntries	Premières entrées
FirstEntriesFlux	Flux des premières entrées
FirstEntriesFluxPM	Flux des premières entrées par membre
Forward	Nombre de jours jusqu'à la date de prévision 
fr	Français
Group	Entrez les pseudos de votre groupe, ligne par ligne, ou laissez la liste vide si vous n'avez pas de groupe :
group	le groupe
Hash	Hash
history	Entrées / sorties de la toile de confiance
hour	heure(s)
In	Entrée
Index	Menu
index	menu
24infosCertif	Propriétés : Informations de certification
infosCertif	Informations de certification
intCertifs	certifications internes
IsntMemberLoc	^0 n'est pas membre (dans ^1, ligne ^2)
IsntMemberNorMissing	^0 n'est ni membre ni exclu (ligne ^1)
IsntMemberNorMissingLoc	^0 n'est ni membre ni exclu (dans ^1, ligne ^2)
IsUnknown	^0 est inconnu(e) (ligne ^1)
IsUnknownLoc	^0 est inconnu(e) (dans ^1, ligne ^2)
KO	KO
language	Choix de la langue
LastApplication	Dernière demande d'adhésion
LCCertifiers	Elle a été certifiée par 
LCComplement	Cette identité va bientôt manquer de certifications à la date suivante :
LimitsC	Limites des certifications
LimitDate	Date limite
32limitsCerts	Limites : Dates limites des certifications
limitsCerts	Dates limites des certifications
LimitsM	Limites des adhésions
30limitsMember	Limites : Dates limites des adhésions
limitsMember	Dates limites des adhésions
31limitsMissing	Limites : Dates limites des adhésions non-renouvelées
limitsMissing	Dates limites des adhésions non-renouvelées
limitsSubCerts	Date à laquelle l'identité affichée manquera de certifications
limitsSubMember	Date à laquelle l'adhésion du membre affiché (avec sa règle de distance) ne sera plus valide
limitsSubMissing	Date à laquelle l'identité exclue affichée sera définitivement révoquée
LMCertifiers	certifié(e) par
LMComplement	Pour rester membre, cette identité doit renouveler son adhésion :
Losses	Pertes
LossesFlux	Flux de pertes
LossesFluxPM	Flux de pertes par membre
Mean	Moyenne
Median	Médiane
MEMBER	MEMBRE
53memberIdentities	Informations : Liste des membres
Members	Membres
404membersCountFlux	Evolution : Flux de membres (graphique)
405membersCountFluxPM	Evolution : Flux de membres par membre (graphique)
403membersCountG	Evolution : Nombre de membres (graphique)
400membersCountT	Evolution :  Nombre de membres (liste)
407membersFEFlux	Evolution : Flux des premières entrées (graphique)
408membersFEFluxPM	Evolution : Flux des premières entrées par membre (graphique)
406membersFirstEntryG	Evolution : Premières entrées (graphique)
401membersFirstEntryT	Evolution : Premières entrées (liste)
MembersFlux	Flux de membres
MembersFluxPM	Flux de membres par membre
410membersLossFlux	Evolution : Flux de pertes (graphique)
411membersLossFluxPM	Evolution : Flux de pertes par membre (graphique)
409membersLossG	Evolution : Pertes (graphique)
402membersLossT	Evolution : Pertes (liste)
MembersNb	Nombre de membres
33memLim	Limites : Limites des adhésions par date
minApplicationDate	Au moins deux mois d'attente après la dernière adhésion (^0)
minute	minute(s)
MISSING	EXCLU(E)
Missing	Exclu(e)s en attente de réadhésion
52missingIdentities	Informations : Liste des identités exclues en attente de réadhésion
MissingNb	Nombre des exclu(e)s
Missings	Exclu(e)s
month	mois
neededCerts	certifications nécessaires :
Never	Jamais
NEWCOMER	ARRIVANT(E)
54newcomerIdentities	Informations : Liste des arrivant(e)s
Newcomers	Arrivant(e)s
newcomers	dossiers en attente
NewcomersNb	Nombre des arrivant(e)s
Nickname	Pseudo
no	non
noGroupL	Ignorer le groupe
NotMembers	Non membres
OK	OK
Out	Sortie
Para_avgGenTime	Temps moyen d'écriture d'un bloc (temps attendu)
Para_c	Croissance relative du DU pour chaque période [dtReeval]
Para_dt	Durée entre deux DU
Para_dtDiffEval	Nombre de blocs exigé avant de pouvoir réévaluer la difficulté minimale
Para_dtReeval	Durée entre deux réévaluations du DU
Para_idtyWindow	Durée maximale d'attente en piscine d'une identité avant expiration
Para_medianTimeBlocks	Nombre de blocs pour le calcul du temps moyen
Para_msPeriod	Durée minimale entre deux demandes d'adhésion d'un membre
Para_msValidity	Durée maximale d'une adhésion
Para_msWindow	Durée maximale d'attente en piscine d'une demande d'adhésion avant expiration
Para_percentRot	Proportion de membres calculants non-exclus de la preuve de travail
Para_sigPeriod	Durée minimale entre deux certifications d'un même membre
Para_sigQty	Nombre minimal de certifications obtenues nécessaire pour être membre
Para_sigStock	Nombre maximal de certifications valides émises par membre
Para_sigValidity	Durée maximale de validité d'une certification
Para_sigReplay	Durée minimale avant le renouvellement d'une certification
Para_sigWindow	Durée maximale d'attente en piscine d'une certification avant expiration
Para_stepMax	Distance maximale entre un membre et [xpercent] des membres référents
Para_txWindow	Durée maximale d'attente en piscine d'une transaction avant expiration
Para_ud0	DU(0) : Dividende Universel initial
Para_udReevalTime0	Date de la première réévaluation du DU
Para_udTime0	Date du premier DU
Para_xpercent	Proportion minimale de membres référents à atteindre pour respecter la règle de distance
Parameters	Paramètres
pending	En cours de réadhésion
PendingApplication	Demande d'adhésion en attente
Permutations	Permutations
permutations	permutations
PermutationsNb	Nombre de permutations : 
PresentCertified	Identités actuellement ou prochainement (°) certifiées :
PresentCertifiers	Actuels ou prochains (°) certificateurs :
Proba	Probabilité
Pseudos	Pseudonymes
51pseudos	Informations : Liste de tous les pseudonymes
PseudosNb	Nombre de pseudonymes
Pubkey	Clef publique
21qualities	Propriétés : Qualités
qualities	Qualités
Quality	Qualité
QualityE	Qualité attendue
QualOfIs	La qualité attendue de ^0 est de ^1%
QualOfUnknownIs	La qualité attendue est de ^1%
remainingCerts	Certifications restantes
requiredCertsNb	^0 certifications, ^1 nécessaires pour la règle de distance
REVOKED	RÉVOQUÉ(E)
Revoked	Révoqué(e)
51revokedIdentities	Informations : Liste des identités révoquées
RevokedM	Identités révoquées
RevokedNb	Nombre d'identités révoquées
Revokeds	Révoqué(e)s
SDev	Écart type
second	seconde(s)
Select	Sélectionner
SentCertsHistory	Évolution des certifications émises
60sentCertsHistory	Statistiques : Évolution des certifications émises
Sentries	Membres référents
57sentries	Informations : Liste des membres référents
SentriesNb	Nombre de membres référents
Sentry	Référent
Server	Version serveur : 
ShowFile	Fichier
SortedByCExpDates	Tri par dates d'expiration des certifications
SortedByCExpDatesL	Tri par dates d'expiration des certifications (→ : date limite)
Status	Statut
Threshold	Seuil
TypeUidOrPubkey	Début de pseudo ou de clef publique
Uid	Pseudo de l'identité considérée (laissez vide s'il s'agit d'un arrivant) :
uid	l'identité considérée
Utc	Temps réel de la chaîne de blocs
ValIdHeads	Valeurs obtenues, par ordre décroissant, pour différents certificateurs supplémentaires :
WarnNum	Nombre d'alertes :
Written_block	Première adhésion
wwByDate	Tri par dates
wwByName	Tri par noms
01wwFile	Previsiones : Fichero preparatorio
wwMeta	Métadonnées
02wwPerms	Prévisions : Permutations
00wwView	Prévisions : Fenêtre WotWizard
year	année(s)
yes	oui
